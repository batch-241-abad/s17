//alert("hello");

/*
	FUNCTIONS
	functions in js are lines or blocks of codes that tell our device or application to perform a certain task when called or invoked

	functions are mostly created to creat complicated task to run several lines of code in succession


	functions are also used to prevent repeating lines or blocks of codes that perfoem the same task ofr functions

	FUNCTION DECLARATION
	(function statement) defines a function with the specified paramaeters



	Syntax:
		function functionName(){
			codeblock(statement)
		}


	function keyword - used to defined a javascript functions
	funtionName - the function name.Function are named to be able to use later in the code

	function block ({}) - the statements which comprise the body of the function. This is where the code to be executed
*/


function printName(){
	console.log("My name is John!");
}


// function invocation: 
printName();

// function declaration vs function expressions

/*
	function declaration - function can be created through function declaration by using the function keyword and adding the function name

	declared fucntion are not executed immediately. they are "saved for later use" and will be executed later when they are invoked(called)
*/
declaredFunction();
function declaredFunction(){
	console.log("Hello World from the declaredFunction()");
}
declaredFunction();

/*
	function expression
		a function can also be stored in a variable. this called function expression

		a function expression is an anonymous function assigned to the variableFunction

		anonymous function is a function withour a name
*/

let variableFunction = function(){
	console.log("Hello from the variableFunction");
}
variableFunction();


let funcExpression = function funcName(){
	console.log("Hello from the other side!")
}

funcExpression();

// calling funcName will case an error
// function expressions are ALWAYS invoked or called using the variable name.

// can we reassign declared funtions and function expressions to a NEW anonymous functions?

// answer if yes if you use let, no if you use const

declaredFunction = function(){
	console.log("updated declared function");
}
declaredFunction();

funcExpression = function (){
	console.log("updated funcExpression");
}
funcExpression();



// function scoping
/*
	scope is the accessibility or visibility of variables
	JS variables has 3 types of scope:
		local or block, global, fucntion scope
*/

{
	let localVar = "I am a local variable"
}

let gloabalVar="I am a global variable"
console.log(gloabalVar);

/*
	this will cause an error:
		console.log(localVar)
*/

function showNames(){
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst)
	console.log(functionLet);
}
showNames();

/*
	this will cause an error:


			function showNames(){
				var functionVar = "Joe";
				const functionConst = "John";
				let functionLet = "Jane";

			}

				console.log(functionVar);
				console.log(functionConst)
				console.log(functionLet);

				showNames();
*/


// NESTED FUNCTION: function inside a function

function myNewFunction(){
	let name="Maria";
	function nestedFunction() {
		let nestedName = "Jose"
		console.log(name);
	}
	/*
		this will cause an error:
		console.log(nestedName);
	*/ 
	nestedFunction();
}
	/*
		this will cause an error:
		console.log(nestedName);
	*/ 
myNewFunction();




// FUNCTION and GLOBAL SCOPED VARIABLES

	// GLOBAL VARIABLE:
	/*let globalName = "Erven";

	function myNewFunction2(){
		let nameInside = "Jenno";
		console.globalName();
	}
	myNewFunction2();

	/*
		This will cause an error:
			console.log(nameInside);
	*/




	// USING ALERT()
	// alert("hello");

	// function showSampleAlert(){
	// 	alert("Hello user!");
	// }
	// showSampleAlert();

	// console.log("I will only log in the consele when the alert is dismissed!");

	// using prompt method

	/*
		Syntax:
			prompt("<DialogInString>");	
	*/
	// let samplePrompt = prompt("enter your name:");
	// console.log("hello, "+samplePrompt);

	// function printWelcomeMessage(){
	// 	let firstName = prompt("Enter your First Name:");
	// 	let lastName = prompt("Enter your Last Name: ");
	// 	console.log("Hello " + firstName + " " + lastName + "!");
	// 	console.log("Welcome to my page!")
	// 	/*
	// 		wont cause and error: 
	// 			console.log("Hello " + firstName + " " + lastName + "!");
	// 			console.log("Welcome to my page!")
	// 	*/
	// }
	// printWelcomeMessage();


	/*
		function naming conventions:
		name your function in small caps. Follows camelCase when naming variables and functions
	*/

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000")
}

displayCarInfo();

// function names should be definitive of the task it will perform. it usually contains a verb

/*
	avoid generic names to avoid confusion within your code: 
	
*/