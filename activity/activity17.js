/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printNameAgeCity = function funcNameAgeCity(){
	let name = prompt("Enter your name:"); 
	let age = prompt("Enter your age:"); 
	let city = prompt("Enter your city:");

	console.log("Hello, "+name+"\nYou are " + age + " years old.\nYou live in " + city); 
	alert("Thank you for your input");
}
printNameAgeCity();


let printFavBand = function funcFavBand(){
	alert("Please enter your favorite bands.");
	let band1 = prompt("Enter your 1st favorite band:");
	let band2 = prompt("Enter your 2nd favorite band: ");
	let band3 = prompt("Enter your 3rd favorite band: ");
	let band4 = prompt("Enter your 4th favorite band: ");
	let band5 = prompt("Enter your 5th favorite band: ");


	console.log("Your favorite bands are:");
	console.log("1. "+band1+"\n2. "+band2+"\n3. "+band3+"\n4. "+band4+"\n5. "+band5);
	alert("Thank you for your input");
}
printFavBand();



let printFavMovies = function funcFavMovies(){
	alert("Please enter your favorite movies.");
	let movie1 = prompt("Enter your 1st favorite movie:");
	let movie2 = prompt("Enter your 2nd favorite movie: ");
	let movie3 = prompt("Enter your 3rd favorite movie: ");
	let movie4 = prompt("Enter your 4th favorite movie: ");
	let movie5 = prompt("Enter your 5th favorite movie: ");

	console.log("Your favorite movies are:");
	console.log("1. "+movie1+"\nRotten Tomatoes Rating: 97%");
	console.log("2. "+movie2+"\nRotten Tomatoes Rating: 98%");
	console.log("3. "+movie3+"\nRotten Tomatoes Rating: 93%");
	console.log("4. "+movie4);
	console.log("Rotten Tomatoes Rating: 94%");
	console.log("5. "+movie5);
	console.log("Rotten Tomatoes Rating: 91%");
	alert("Thank you for your input");
}
printFavMovies();

let printFriends = function printFriends(){
	alert("Please enter your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}
printFriends();